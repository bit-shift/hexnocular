/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <map>

#include <boost/optional/optional.hpp>

#include <RtMidi.h>

namespace hexnocular {
namespace midi {

//----------------------------------------------------------------------------------------------------------------------

enum class EventType {
    note_on,
    note_off
};

struct Event {
    EventType   type;
    size_t      channel;
    size_t      note;
    size_t      velocity;
};

using Sequences = std::map< size_t, Event >;


//----------------------------------------------------------------------------------------------------------------------

namespace {

RtMidiIn    midi_in( RtMidi::Api::LINUX_ALSA, "hexnocular", 1000 );
Sequences   sequences;

}


//----------------------------------------------------------------------------------------------------------------------

namespace in {

void init()
{
    midi_in.openPort( 0, "hexnocular MIDI Input 0");
}


//----------------------------------------------------------------------------------------------------------------------

std::string convert_message( std::vector<unsigned char>& message )
{
    std::string converted_message;

    for( auto& monster: message )
    {
        converted_message += (char)monster;
        std::cout << (unsigned)monster << std::endl;
    }
    return converted_message;
}

//----------------------------------------------------------------------------------------------------------------------

Event create_event( std::vector<unsigned char>& message )
{
    Event event;

    event.type      = (size_t)message[0] == 128 ? EventType::note_off : EventType::note_on;
    event.note      = (size_t)message[1];
    event.velocity  = (size_t)message[2];

    return event;
}


//----------------------------------------------------------------------------------------------------------------------

boost::optional<Event> next_event()
{
    std::vector<unsigned char> midi_message;

    midi_in.getMessage( &midi_message );

    if( !midi_message.empty() )
    {
        return create_event( midi_message );
    }
    return boost::none;
}


//----------------------------------------------------------------------------------------------------------------------

void update_sequences()
{
    auto next = next_event();

    if( next )
    {
        auto event = next.value();

        if( event.type == EventType::note_on )
        {
            sequences.insert( std::make_pair( event.note, event ) );
        }
        else if ( event.type == EventType::note_off )
        {
            auto it = sequences.find( event.note );

            if( it != sequences.end() )
                sequences.erase( it );
        }
    }
}


//----------------------------------------------------------------------------------------------------------------------

Sequences active_sequences()
{
    return sequences;
}


//----------------------------------------------------------------------------------------------------------------------

} // namespace in
} // namespace midi
} // namespace hexnocular
