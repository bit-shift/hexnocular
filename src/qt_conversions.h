/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QPolygon>
#include <QPoint>

namespace hexnocular {
namespace data {

using Point = QPoint;

// ---------------------------------------------------------------------------------------------------------------------

//QPoint q_point( Point point )
//{
//    return QPoint( point._x, point._y );
//}

//Point point( QPoint point )
//{
//    return Point{ point.rx(), point.ry() };
//}

// ---------------------------------------------------------------------------------------------------------------------

//    QPolygon q_polygon( Polygon polygon )
//    {
//        return static_cast<QPolygon>( polygon );
//    }

//    Polygon q_polygon( QPolygon polygon )
//    {
//        return static_cast<QPolygon>( polygon );
//    }

}
}
