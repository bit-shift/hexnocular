/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "WorkspaceImage.h"

#include <iostream>
#include <thread>
#include <mutex>
#include <math.h>

#include <boost/optional/optional_io.hpp>

#include <QPainter>
#include <QPolygon>
#include <QQuickWindow>

#include "algorithm.h"
#include "qt_conversions.h"
#include "midi.h"

namespace hexnocular {

// --------------------------------------------------------------------------------------------------------------------

WorkspaceImage::WorkspaceImage( QQuickItem* parent )
    : QQuickPaintedItem( parent ),
      _pen_white( Qt::white ),
      _pen_red( Qt::red ),
      _pen_green( Qt::green ),
      _tick( 0 )
{
    // Render OpenGL
    setRenderTarget( QQuickPaintedItem::FramebufferObject );

    _pen_white.setWidth( 5 );
    _pen_red.setWidth( 5 );
    _pen_green.setWidth( 5 );

    _current_pen = _pen_white;

    midi::in::init();
}


// --------------------------------------------------------------------------------------------------------------------

void WorkspaceImage::paint( QPainter *painter )
{
    auto dimension  = QPoint( window()->width(), window()->height() );

    midi::in::update_sequences();

    for( auto& sequence: midi::in::active_sequences() )
    {
        auto event  = sequence.second;
        auto pen    = QPen( QColor( event.note * 2, 238, 170, 127) );

        pen.setWidth( 5 );

        painter->setPen( pen );
        painter->drawEllipse( algorithms::center( dimension ), event.note * 4, event.note * 4 );
    }


}


// --------------------------------------------------------------------------------------------------------------------

void WorkspaceImage::setTick( const int& tick )
{
    _tick = tick;

    tickChanged();
    update();
}


// --------------------------------------------------------------------------------------------------------------------

} // namespace freemapper
