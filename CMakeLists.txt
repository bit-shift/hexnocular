cmake_minimum_required(VERSION 3.0.0)

project(hexnocular)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wpedantic -D__LINUX_ALSA__")

set(CMAKE_AUTOMOC ON)

set(SOURCES_DIR src)
set(HEADERS ${SOURCES_DIR})

set(RTMIDI_DIR ${CMAKE_CURRENT_SOURCE_DIR}/libs/rtmidi)

aux_source_directory(${SOURCES_DIR} SRC_LIST)
aux_source_directory(${RTMIDI_DIR} RTMIDI_LIST)

include_directories(${SOURCES_DIR} ${RTMIDI_DIR})

# TODO find better solution to include headers.
add_executable(${PROJECT_NAME} ${SRC_LIST} ${RTMIDI_LIST}
    ${HEADERS}/data.h
    ${HEADERS}/animation.h
    ${HEADERS}/algorithm.h
    ${HEADERS}/qt_conversions.h
    ${HEADERS}/midi.h)

###############################################################################
### Qt
###############################################################################
# Instruct CMake to run moc automatically when needed.
#set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Qml REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(Qt5Gui REQUIRED)

# Use the Widgets module from Qt 5.
target_link_libraries(${PROJECT_NAME} Qt5::Widgets)
target_link_libraries(${PROJECT_NAME} Qt5::Quick)

configure_file(qml/main.qml qml/main.qml)

qt5_use_modules(${PROJECT_NAME} Widgets)
qt5_use_modules(${PROJECT_NAME} Qml)
qt5_use_modules(${PROJECT_NAME} Quick)
qt5_use_modules(${PROJECT_NAME} Gui)

###############################################################################
### boost
###############################################################################
find_package(Boost REQUIRED)
target_link_libraries(${PROJECT_NAME} boost_system)

###############################################################################
### RtMidi
###############################################################################
find_package(ALSA REQUIRED)
find_package(Threads REQUIRED)

target_link_libraries(${PROJECT_NAME} asound)
target_link_libraries(${PROJECT_NAME} pthread)
